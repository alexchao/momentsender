# Description:
MomentSender is Touchcloud's KekkAI endpoints
It can automatically notify Kekkai moment newest information to you by Email & Line 


# Installation:
```
    git clone git@gitlab.com:touchcloud/samples/third-party/kekkai.git
```
# Usage:
```
- cd momentsender
- cd bin
- ./momentSender -emailAccount user@gmail.com -emailPassword password -email receiver1@gmail.com -email receiver2@yahoo.com.tw 
- ./momentSender -lineWebhook http://6a5e838b.ngrok.io/callback -line U2ddbded68f4ebb8edd4412c4d4a7e8c7

- also you can use both
- ./momentSender -lineWebhook -line -emailAccount -emailPassword -email
```
* `-emailAccount`: Your Gmail Account
* `-emailPassowrd`: Your Gmail Password
* `-email`: Email Receiver

* `-lineWebhook`: Your LineBot Webhook
* `-line`: momentSender linebot offer's LineID


# Notice:
## 如果你想要使用Email通知功能
1. 首先你必須登入發信之Gmail帳號
2. 點選右上個人圖像,管理你的google帳戶
3. 點選左方側欄安全性之選項
4. 低安全性應用程式存取權 設置為開啟

## If you want to use email notification
1. first you have to go to gmail and login sender mail
2. Click on the top right personal image to manage your google account
3. Click the security option on the left sidebar
4. Low Security Application Access Set to On


## 如果你想要使用Line通知功能,有兩塊步驟需先完成

### LineBot Server
#### linebot Part:
- 請查看LineBot子專案資料夾內容


### LineBot Client
1. 在註冊完且確認LineBot可正常運作後,加入LineBot機器人好友,或將LineBot加入至群組內
2. 在對話欄任意發送一句話,LineBot將回覆屬於你或群組之 MomentSender LineID
3. 在執行MomentSender之程式時後綴加上 -line LineID 即可發送通知至該ID

## If you want to use Line notification, there are two parts to complete first
### LineBot Server
- Please look linebot subProject directory

### LineBot Client
1. After registering and confirming that LineBot can work normally, join a LineBot robot friend, or add LineBot to a group
2. Send any sentence in the Chat box, LineBot will reply to the MomentSender LineID belonging to you or the group
3. When running MomentSender's program, add the -line LineID suffix that you can send a notification to that ID


