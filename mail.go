package main

import (
	"crypto/tls"
	"errors"
	"net/smtp"
	"strings"

	"gopkg.in/gomail.v2"
)

type loginAuth struct {
	username, password string
}

func LoginAuth(username, password string) smtp.Auth {
	return &loginAuth{username, password}
}

func (a *loginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte{}, nil
}

func (a *loginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		switch string(fromServer) {
		case "Username:":
			return []byte(a.username), nil
		case "Password:":
			return []byte(a.password), nil
		default:
			return nil, errors.New("Unkown fromServer")
		}
	}
	return nil, nil
}

var mailAccount string
var mailPassword string
var mailReceivers arrayFlags

type emailData struct {
	account   string
	password  string
	host      string
	receiver  []string
	content   string
	imagePath string
}

func NewEmail(message, imgPath string) emailData {
	return emailData{
		//host:      "smtp.office365.com",
		host:      "smtp.gmail.com",
		account:   mailAccount,
		password:  mailPassword,
		receiver:  mailReceivers,
		content:   message,
		imagePath: imgPath,
	}
}

func seperateAccount(account string) (username, host string) {
	if len(strings.Split(account, "@")) < 2 {
		sugarLogger.Error("Failed to split account")
	} else {
		username = strings.Split(account, "@")[0]
		host = strings.Split(account, "@")[1]
	}
	return username, host
}

func sendEmail(e emailData) {
	m := gomail.NewMessage()
	m.SetHeader("From", e.account)
	m.SetHeader("To", e.receiver...)
	//m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	m.SetHeader("Subject", "MomentSender")
	m.SetBody("text/html", e.content)
	m.Attach(e.imagePath)

	auth := LoginAuth(mailAccount, mailPassword)
	d := gomail.NewDialer(e.host, 587, e.account, e.password)
	d.Auth = auth
	d.TLSConfig = &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         e.host,
	}

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		sugarLogger.Error(err)
	}

}
