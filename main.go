package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

type Value interface {
	String() string
	Set(string) error
}

type arrayFlags []string

var currentDirectory, _ = os.Getwd()

func (i *arrayFlags) String() string {
	return fmt.Sprint(*i)
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func init() {
	flag.Var(&mailReceivers, "email", "Mail Receiver")
	flag.StringVar(&mailAccount, "emailAccount", "", "Mail Account")
	flag.StringVar(&mailPassword, "emailPassword", "", "Mail Password")

	flag.Var(&LineIDs, "line", "Line ID")
	flag.StringVar(&LineWebhook, "lineWebhook", "http://6a5e838b.ngrok.io/callback", "Line Webhook")

	flag.Var(&messages, "message", "Phone message")
}

func momentSender(context *gin.Context) {
	context.String(http.StatusOK, "Touchcloud kekkai moment")

	b, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		sugarLogger.Error("Read failed:", err)
	}
	var in []kekkAI

	err = json.Unmarshal(b, &in)
	sugarLogger.Info(in, "\n\n")

	for _, item := range in {
		imgName := item.Data.Uuid + ".jpg"
		imgPath := currentDirectory + "/" + imgName
		Base64ToJpeg(item.Data.Snapshot, imgPath)

		msgContent := "Camera Name:" + item.Data.CamName + "\nItem Name: " + item.Data.Item + "\nLabel: " + item.Data.Label + "\nTime: " + item.Data.Time

		if len(mailReceivers) > 0 {
			newEmail := NewEmail(msgContent, imgPath)
			sendEmail(newEmail)
		}
		if len(LineIDs) > 0 {
			img, err := os.Open(imgPath)
			if err != nil {
				sugarLogger.Error(err)
			}
			imgurl := upload(img, accessToken)

			newLine := NewLine(msgContent, imgurl)
			sendToLine(newLine, LineWebhook)
		}
		if len(messages) > 0 {
			sugarLogger.Info("Do something.")
		}

		os.Remove(imgPath)
	}
}

func main() {
	InitLogger()
	flag.Parse()
	defer sugarLogger.Sync()
	router := gin.Default()
	router.POST("/sender", momentSender)
	router.Run(":8001")
}
