package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"

	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/linebot"
)

type momentRequset struct {
	Message string   `json:"message"`
	ImgUrl  string   `json:"imgUrl"`
	LineID  []string `json:"lineID"`
}

var CHANNEL_SECRET string
var CHANNEL_TOKEN string

var bot *linebot.Client
var err error

func init() {
	flag.StringVar(&CHANNEL_SECRET, "secret", "29a083c66592adb3dccbd56dce7a64e3", "LineBot CHANNEL_SECRET")
	flag.StringVar(&CHANNEL_TOKEN, "token", "jnZHPJnyQ91tURTcxfsdxHQXvlqd4OaaFHYfvYGj3I8Jg5w6CAMwHXANpy0FhTdqUpsZw7xaKKzQTuIiXoRD7FKzfWCYQQeCYwyO7iferqoA1cUedMoyCvjQkrJgUMdSXhCPFHRgicQ/ruUwCfv0uQdB04t89/1O/w1cDnyilFU=", "LineBot CHANNEL_TOKEN")
}

func lineMessage(context *gin.Context) {
	var momentReq momentRequset
	var in map[string]interface{}

	b, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		sugarLogger.Error("Failed to read: ", err)
	}
	defer context.Request.Body.Close()
	err = json.Unmarshal(b, &in)
	if err != nil {
		sugarLogger.Error("Failed to unmarshal: ", err)
	}

	sugarLogger.Info(in)

	if _, ok := in["imgUrl"]; ok {
		err = json.Unmarshal(b, &momentReq)
		for _, id := range momentReq.LineID {
			if bot.PushMessage(id, linebot.NewTextMessage(momentReq.Message)).Do(); err != nil {
				sugarLogger.Error("Failed to pushMessage: ", err)
			}
			if bot.PushMessage(id, linebot.NewImageMessage(momentReq.ImgUrl, momentReq.ImgUrl)).Do(); err != nil {
				sugarLogger.Error("Failed to pushMessage: ", err)
			}
		}
	} else {
		request := &struct {
			Events []*linebot.Event `json:"events"`
		}{}
		err = json.Unmarshal(b, request)

		for _, event := range request.Events {
			if event.Type == linebot.EventTypeMessage {
				switch event.Message.(type) {
				case *linebot.TextMessage:
					switch event.Source.Type {
					case "user":
						if _, err = bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("你的ID:"+event.Source.UserID)).Do(); err != nil {
							sugarLogger.Error("Failed to replyMessage: ", err)
						}

					case "group":
						if _, err = bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("群組ID:"+event.Source.GroupID)).Do(); err != nil {
							sugarLogger.Error("Failed to replyMessage: ", err)
						}

					}
				}
			}
		}
	}

}

func main() {
	InitLogger()
	flag.Parse()

	bot, err = linebot.New(
		CHANNEL_SECRET,
		CHANNEL_TOKEN,
	)

	fmt.Println("Linebot server is running...")

	router := gin.Default()
	router.POST("/callback", lineMessage)
	router.Run(":8000")

}
