# MomentSender - linebot server

## Description:
MomentSender is Touchcloud's kekkai endpoint,
and this project is momentSender's linebot server,
which can distribute Specific LineID, and push notification.

## Requirement:
- ngrok  (line Develop Webhook need)

## Usage:

```
- cd linebot
- cd bin
- ./linebot -secret Channel_secret -token Channel_token
```

* `-secret`: Your LineBot Channel_secret
* `-token`: Your LineBot Channel_token

### LineBot Server
#### linebot server Part1:
1. 首先你必須註冊一個官方 Line Develop 帳號,進入 https://developers.line.biz/zh-hant/ ,並登入使用之Line帳號(Log in with Line account)
2. 建立新的Provider,機器人名字可自取
3. 建立完後,Channels處選擇 Create a Messaging API channel
4. Channel name, Channel description, Category, Email Address 處可自填,填完後按下Create
5. 建立完Channel後, Basic settings中, Channel secret 需記起來供之後server啟動所用
6. 接著點選Messaging API選項, 其中之 Bot basic ID 及 QRcode 為日後加入LineBot時所需
7. 點選 Allow bot to join group chats 旁之Edit, Group and multi-person chats 改選為 Allow account to join groups and multi-person chats
8. 點選 Auto-reply messages  旁之Edit, Auto-response 改選為 Disabled, Webhook 改選為 Enable(Part2時填寫Webhook網址)
9. 回Messaging API選項,最後之 Channel access token 點擊issue,並記下token供之後server啟動所用

#### linebot server part2:
LineBot的webhook必須填寫https規格之網域,官方建議使用 Heroku 架設server網域,本文使用 ngrok 建立簡易網域
1. 首先你必須註冊 ngrok 之帳號,進入 https://ngrok.com/ ,註冊並登入後,在user設定 Auth 處找到 Authtoken並記下來
2. 接著進入 https://ngrok.com/download 跟著官方步驟下載並安裝 ngrok
3. 開啟bash 輸入 ./ngrok authtoken <YOUR_AUTH_TOKEN>
4. 最後輸入 ./ngrok http 8000
5. 擷取 ngrok之 Forwarding https 之網址 如 https://6a5e838b.ngrok.io
6. 回到 LineBot Messaging API, Webhook URL處填上 https://6a5e838b.ngrok.io/callback 並按下Update,即完成LineBot與網域之串連

### LineBot Server
#### linebot server Part1:
1. First you must register for an official Line Develop account, go to https://developers.line.biz/zh-hant/, and log in to the used Line account (Log in with Line account)
2. Create a new Provider, the name of the robot can be self-selected
3. After creating, select Create a Messaging API channel at Channels
4. Channel name, Channel description, Category, Email Address can be self-filled, after finished, press Create
5. After establishing the Channel, in the basic settings, the Channel secret needs to be remembered for later server startup
6. Then click on the Messaging API option, where the Bot basic ID and QRcode are required when adding LineBot in the future
7. Click Edit next to Allow bot to join group chats and change Group and multi-person chats to Allow account to join groups and multi-person chats
8. Click Edit next to Auto-reply messages, change Auto-response to Disabled, Webhook to Enable(fill in Webhook URL when Part2)
9. Go back to the Messaging API option, click Channel token issue at the end, and note down the token for the server to start later

#### linebot server part2:
LineBot's webhook must fill in the https domain.Officially, it is recommended to use Heroku to set up the server domain. This project uses ngrok to create a simple domain.
1. First you must register an ngrok account, go to https://ngrok.com/, after registering and logging in, find the Authtoken in the user setting Auth and write it down
2. Then go to https://ngrok.com/download Follow the official steps to download and install ngrok
3. Open bash and type ./ngrok authtoken <YOUR_AUTH_TOKEN>
4. Then type ./ngrok http 8000
5. Retrieve ngrok's Forwarding https URL such as https://6a5e838b.ngrok.io
6. Back to the LineBot Messaging API, fill in the Webhook URL https://6a5e838b.ngrok.io/callback and press Update to complete the connection between LineBot and the domain(don't miss callback)

## Notice:
如果你是使用Ngrok架設LineBot網域,每當Ngrok重啟一次,則需更改LineBot之Webhook網址

If you use Ngrok to set up a LineBot domain, you need to change the WebHook URL of LineBot whenever Ngrok restarts
