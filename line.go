package main

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type lineData struct {
	Message string   `json:"message"`
	ImgUrl  string   `json:"imgUrl"`
	LineID  []string `json:"lineID"`
}

var LineIDs arrayFlags
var LineWebhook string

func NewLine(message, imgUrl string) lineData {
	return lineData{
		Message: message,
		ImgUrl:  imgUrl,
		LineID:  LineIDs,
	}
}

func sendToLine(data lineData, url string) {
	jsonstr, err := json.Marshal(data)
	if err != nil {
		sugarLogger.Error(err)
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonstr))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		sugarLogger.Error(err)
	}
	defer resp.Body.Close()

	sugarLogger.Info("response Status:", resp.Status)
	sugarLogger.Info("response Headers:", resp.Header)
}
